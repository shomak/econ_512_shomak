function [v,w] = lifesucks2(b)
load('hw3.mat')
v=sum(exp(X*b) - y.*(X*b) + log(factorial(y)));
if nargout>1
    vf = @(q)sum(exp(X*q) - y.*(X*q) + log(factorial(y)));
    w = fdjac(vf, b)';
end
end
