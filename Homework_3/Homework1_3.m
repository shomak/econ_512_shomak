%Question 1
%Part a, b : calculations done in latex file
%No coding required

%Part c: Maximum Likelihood
data = gamrnd(3,2,[1000,1]);
initial_value = 1;
[theta_ml, var_covar] = gammalikelihood(data, initial_value);

%Part d: Plotting the graph
estimates =[];
Y=[];
for i=1:1000
    x = gamrnd(3,2,[1000,1]);
    Y1 = sum(x)/length(x);
    Y2 = exp((sum(log(x))/length(x)));
    Y(i) =Y1/Y2;
    theta = gammalikelihood(x,initial_value);
    estimates(i) = theta(1);
end
fig=scatter(Y, estimates, 'filled')
saveas(fig, 'graph.png')


%Question 2 
%part a
%fminunc without derivative method
options_unc = optimoptions(@fminunc,'Display','iter','Algorithm','quasi-newton');
[ml_est_unc, ml_value_unc, ml_exitflag_unc, ml_output_unc,~, ml_hessian_unc] = fminunc(@lifesucks2, [0;0;0;0;0;0], options_unc)
%fminunc with derivative provided method
options_ml = optimoptions('fminunc','Display','iter','Algorithm','trust-region','SpecifyObjectiveGradient',true);
[ml_est_uncj,ml_value_uncj, ml_exitflag_uncj, ml_output_uncj,~,ml_hessian_unc] = fminunc(@lifesucks2, [0;0;0;0;0;0], options_ml)
%nelder-mead method
load('hw3.mat')
mlObjFun = @(beta) sum(-exp(X*beta) + y.*(X*beta) - log(factorial(y)));
ml_est_nm = neldmead(mlObjFun,[0;0;0;0;0;0]);

% NM does not wirk if you have discontinuity in objective

%bhhh method
[ml_est_bhhh,ml_iterates_bhhh,ml_inithessian_bhhh,ml_hessian_bhhh]=bhhh([0;0;0;0;0;0]);
%Question 2 part b
eigenvalue_initial = eig(ml_inithessian_bhhh);
eigenvalue_final = eig(ml_hessian_bhhh);
%Question 2 part c
load('hw3.mat')
options_unc = optimoptions(@fminunc,'Display','iter','Algorithm','quasi-newton');
[nlls_est_unc,nlls_value_unc, nlls_exitflag_unc, nlls_output_unc,~, nlls_hessian_unc] = fminunc(@nllsfunc, [0;0;0;0;0;0], options_unc)
options_nlls = optimoptions('fminunc','Display','iter','Algorithm','trust-region','SpecifyObjectiveGradient',true);
[nlls_est_uncj,nlls_value_uncj, nlls_exitflag_uncj, nlls_output_uncj, ~,nlls_hessian_uncj] = fminunc(@nllsfunc, [0;0;0;0;0;0], options_nlls)
%Question 2 part d
bhhh_standarderror = diag(sqrt(pinv(ml_hessian_bhhh)))./sqrt(length(y));
D = zeros(6,6);
V = 0;
for ii = 1:length(y)
    D = D + (X(ii,:)'.*exp(X(ii,:)*nlls_est_uncj)^2.*X(ii,:));
    V = V + (y(ii) - exp(X(ii,:)*nlls_est_uncj))^2.*(X(ii,:)'.*exp(X(ii,:)*nlls_est_uncj)^2.*X(ii,:));
end
V = 4/length(y).*V;
D = 1/length(y).*D;
psi = D\V/D;
nlls_standarderror = diag(sqrt(inv(psi)./length(y)));
