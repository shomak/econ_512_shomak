function [estimates, iterates,initial_Hessian, Hessian] = BHHH(initial_beta)
load('hw3.mat')
max_iter=1000;
tol=sqrt(eps);
old_beta = initial_beta;
initial_Hessian = ((y-exp(X*old_beta))*ones(1,6).*X)'*((y-exp(X*old_beta))*ones(1,6).*X);

for i=1:max_iter
    Hessian = ((y-exp(X*old_beta))*ones(1,6).*X)'*((y-exp(X*old_beta))*ones(1,6).*X);
    Jacobian=(sum((y-exp(X*old_beta))*ones(1,6).*X))';
    new_beta=old_beta+Hessian\Jacobian;
    iterates=i;
    if norm(new_beta-old_beta)<tol
        break
    end
    old_beta = new_beta;
end
estimates = new_beta;
end