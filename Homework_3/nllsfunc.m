function [v,w] = nllsfunc(b)
load('hw3.mat')
v=sum((-y + exp(X*b)).^2);
if nargout>1
    vf = @(b) sum((-y + exp(X*b)).^2);
    w = fdjac(vf, b)';
end
end
