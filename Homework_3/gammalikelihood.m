function [theta, var_covar] = gammalikelihood(x, init_theta1)
    tol=sqrt(eps);
    max_iter = 1000;
    arith_mean = sum(x)/length(x);
    geom_mean = (sum(log(x))/length(x));
    foc1 = @(theta1) (log(theta1)-log(arith_mean)-psi(theta1)) + geom_mean;
    fjac1 =@(theta1) (1/theta1) - trigamma(theta1);
    theta1=init_theta1;
    for i=1:max_iter
        fv = foc1(theta1);
        fj = fjac1(theta1);
        theta1 = theta1 - (fj)\fv;
        if norm(fv)<tol
            break;
        end
    end
    theta2 = theta1/arith_mean;
    theta = [theta1, theta2];
    var_covar = pinv(length(x).*[trigamma(theta1), -(1/theta2); -(1/theta2), (theta1/theta2^2)]);
end