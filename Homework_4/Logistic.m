function probability = Logistic(x)
probability = (1+exp(-x)).^(-1);
end