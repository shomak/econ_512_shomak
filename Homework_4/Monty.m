function Expectation = Monty(inputs)
sigma_beta = inputs(1); beta_0=inputs(2); gamma=inputs(3);
load('hw4data.mat')
Y_new = data.Y; Z_new = data.Z; X_new = data.X; N_new = data.N; T_new = data.T;
rng default;
data_points = randn(100,1);
b = sigma_beta.*data_points + beta_0;
for j=1:length(data_points)
hk(j,:) = prod((Logistic(b(j).*X_new + gamma.*Z_new).^(Y_new)).*((1-Logistic(b(j).*X_new + gamma.*Z_new)).^(1-Y_new)));
end
Expectation = -sum(log((sum(hk)./length(data_points))));
end