function Expectation = Monty_general(inputs)

sigma_beta = inputs(1); sigma_u=inputs(2); sigma_betau = inputs(3); beta_0=inputs(4); u_0=inputs(5); gamma=inputs(6);
Sigma=[sigma_beta, sigma_betau; sigma_betau, sigma_u];
Mu=[beta_0, u_0];

load('hw4data.mat')
Y_new = data.Y; Z_new = data.Z; X_new = data.X; N_new = data.N; T_new = data.T;
% if sigma_beta<=0 | sigma_u<=0 | (sigma_beta*sigma_u - (sigma_betau)^2)<=0
%     data_points = rand(100,2)*chol(Sigma) + Mu(ones(100,1),:);
% else
%     rng default;
% data_points = rand(100,2)*chol(Sigma) + Mu(ones(100,1),:);
% end

[~, d]=chol(Sigma);
if d~=0
    [T, D] = eig(Sigma);
    Sigma = T*(diag(max(diag(D))))/T;
end
rng default;
data_points = mvnrnd(Mu, Sigma, 100)*Sigma;
b = data_points(:,1) + beta_0;
u=data_points(:,2) + u_0;
for j=1:length(data_points)
hk(j,:) = prod((Logistic(b(j).*X_new + gamma.*Z_new + u(j)).^(Y_new)).*((1-Logistic(b(j).*X_new + gamma.*Z_new+ u(j))).^(1-Y_new)));
end
Expectation = -sum(log((sum(hk)./length(data_points))));
end