load('hw4data.mat')
Y_new = data.Y; Z_new = data.Z; X_new = data.X; N_new = data.N; T_new = data.T;
data_points = sqrt(2).*sort(roots(hermite_rec(T_new)));
prob_mass = (2^(T_new-1)*factorial(T_new)).*((T_new*hermite_num(T_new-1, (data_points./sqrt(2)))).^(-2));



Uncondi_Like = @(input) Lhood(input);
montemethod =@(input) Monty(input);

a1 = (Uncondi_Like([1,0.1,0]));
a2=Monty([1,0.1,0]);

A=[-1,0,0]; b=0;
g = fmincon(Uncondi_Like, [1,0.1,0], A, b)
h = fmincon(montemethod, [1,0.1,0], A, b)



A = [-1,0,0,0,0,0;0,-1,0,0,0,0; 0,0,0,0,0,0;0,0,0,0,0,0;0,0,0,0,0,0;0,0,0,0,0,0]; b= [0;0;0;0;0;0];
o = fmincon(@Monty_general, [1,4,1.5,0.1,1,0], A, b,[],[],[],[],@positivedev)
 options = optimoptions(@fmincon,'Algorithm','sqp');
%   no such function positivedev. maybe you forgot to include it. 

lowerbound = [ 0,0,-1,-3,-2,-3];
upperbound = [ 5,3,1,5,2,3];

initial_values=[1,1,0.1,0.1,0.1,0];
o = fmincon(@Monty_general,initial_values,[],[],[],[],lowerbound,upperbound,@confuneq);
