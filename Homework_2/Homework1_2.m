%**************************************************************************************************************************************************************
%                               ECON 512A: Empirical Methods Module 1
%                                    Homework 2 Coding Solution
%                                       Shomak Chakrabarti
%**************************************************************************************************************************************************************

%**************************************************************************************************************************************************************
% Last Modified: 22th September 2017, by Shomak Chakrabarti
%**************************************************************************************************************************************************************
clear all;clc;
price_matrix = [[1,1,1];[0,0,0];[1,1,2];[3,2,1]];
% use please the section separator double percent sign
%% **************************************************************************************************************************************************************
% Question 1
%**************************************************************************************************************************************************************
%Parameterizations used for this part : v_j = -1 for all j
%Prices are assumed to be : p_j = 1 for all j

fprintf('Question 1: Demand for Firms:\n\n')
v = -ones(1,3);
Demands = DemandFunctions(v, price_matrix(1,:));
fprintf('The demands of each firm at price (1,1,1) is given by: %0.4f %0.4f %0.4f\n', Demands)
fprintf('\n')

%% **************************************************************************************************************************************************************
% Question 2
%**************************************************************************************************************************************************************
%Parameterizations used for this part : v_j = -1 for all j

fprintf('Question 2: Broyden Method:\n\n')
for i=1:4;
    tic
    [Price_Broyden(i,:),~,~,iteration(i),~] = broyden('Bertrand', price_matrix(i,:)');
    toc
    fprintf('Initial Price: %0.2f %0.2f %0.2f\n', price_matrix(i,:))
    fprintf('Equilibrium Price: %0.4f %0.4f %0.4f\n', Price_Broyden(i,:))
    fprintf('No. of iterations %f\n', iteration(i))
    fprintf('\n')
end

%**************************************************************************************************************************************************************
%% Question 3
%**************************************************************************************************************************************************************
%Parameterizations used for this part : v_j = -1 for all j

fprintf('Qusetion 3: Gauss-Jacobi algorithm with Secant Method in each step:\n\n')
for i=1:4;
    tic
    price_initial1=price_matrix(i,:);
    price_initial2 = 1./(ones(3,1)-DemandFunctions(v,price_initial1));
    [price_GaussJacobi(i,:),iterations_GJ(i)] = GaussJacobi(price_initial1,price_initial2, 0.001,100,100);
    % use the same tolerance as in Broyden. it is defaulted at 1e-8 there
    toc
    fprintf('Initial Price: %0.2f %0.2f %0.2f\n', price_initial1)
    fprintf('Equilibrium Price: %0.4f %0.4f %0.4f\n', price_GaussJacobi(i,:))
    fprintf('No. of iterations %f\n', iterations_GJ(i))
    fprintf('\n')
end



%**************************************************************************************************************************************************************
%% Question 4
%**************************************************************************************************************************************************************
%Parameterizations used for this part : v_j = -1 for all j

fprintf('Qustion 4: Value Function Iteration:\n\n')
for i=1:4;
    tic
    [price_ValueFunctionIteration(i,:), iterations_VI(i)] = ValueIteration(price_matrix(i,:), 0.001, 100);
    toc
    fprintf('Initial Price: %0.2f %0.2f %0.2f\n', price_matrix(i,:))
    fprintf('Equilibrium Price: %0.4f %0.4f %0.4f\n', price_ValueFunctionIteration(i,:))
    fprintf('No. of iterations %f\n', iterations_VI(i))
    fprintf('\n')
end

%**************************************************************************************************************************************************************
%% Question 5
%**************************************************************************************************************************************************************
%Parameterizations used for this part : v_j = -1 for all j

fprintf('Qusetion 5: Gauss-Jacobi algorithm with One-Step-Secant Method in each iteration:\n\n')
for i=1:4;
    tic
    price_initial1=price_matrix(i,:);
    price_initial2 = 1./(ones(3,1)-DemandFunctions(v,price_initial1));
    [price_GaussJacobiOneStep(i,:),iteration_GJO(i)] = GaussJacobi(price_initial1,price_initial2, 0.001,100,1);
    toc
    fprintf('Initial Price: %0.2f %0.2f %0.2f\n', price_initial1)
    fprintf('Equilibrium Price: %0.4f %0.4f %0.4f\n', price_GaussJacobiOneStep(i,:))
    fprintf('No. of iterations %f\n', iteration_GJO(i))
    fprintf('\n')
end



%**************************************************************************************************************************************************************
%**************************************************************************************************************************************************************
