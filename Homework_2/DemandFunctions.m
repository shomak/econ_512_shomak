function [demands] = DemandFunctions(v,p)
    demand_firmA = (exp(v(1)-p(1)))/(1+exp(v(1) - p(1))+exp(v(2) - p(2))+exp(v(3) - p(3)));
    demand_firmB = (exp(v(2)-p(2)))/(1+exp(v(1) - p(1))+exp(v(2) - p(2))+exp(v(3) - p(3)));
    demand_firmC = (exp(v(3)-p(3)))/(1+exp(v(1) - p(1))+exp(v(2) - p(2))+exp(v(3) - p(3)));
    demands=[demand_firmA; demand_firmB; demand_firmC];
    %demands = (1/(1+exp(v(1) - p(2))+exp(v(2) - p(3))+exp(v(3) - p(3)))).*exp(v-p);
end

