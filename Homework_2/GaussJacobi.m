function [price, iteration] = GaussJacobi(init_p1, init_p2,tol,max_iter_GJ,max_iter_Sec)
    iteration=0;
    for i=1:max_iter_GJ
        q_a = @(w1)(1 - w1*(1-(exp(-1 - w1)/(1+ exp(-1 - w1) + exp(-1 - init_p1(2)) + exp(-1 - init_p1(3))))));
        q_b = @(w2)(1 - w2*(1-(exp(-1 - w2)/(1+ exp(-1 - init_p1(1)) + exp(-1 - w2) + exp(-1 - init_p1(3))))));
        q_c = @(w3)(1 - w3*(1-(exp(-1 - w3)/(1+ exp(-1 - init_p1(1)) + exp(-1 - init_p1(2)) + exp(-1 - w3)))));
        w1 = SecantMethod(q_a, init_p1(1), init_p2(1), tol, max_iter_Sec);
        w2 = SecantMethod(q_b, init_p1(2), init_p2(2), tol, max_iter_Sec);
        w3 = SecantMethod(q_c, init_p1(3), init_p2(3), tol, max_iter_Sec);
    
        wn = [w1;w2;w3];
        error = norm(wn - init_p1);
        if (error<tol) 
            break  
        end
        init_p1 = wn;
        iteration=iteration+1;
    end
if (i==max_iter_GJ)
   disp('The algorithm did not converge')
end
price = wn;
end