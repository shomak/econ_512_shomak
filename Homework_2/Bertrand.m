function [foc] = Bertrand(p)
    v = -ones(1,3);
    Demand=DemandFunctions(v,p);
    %foc_firmA =Demand(1) -p(1)*Demand(1)*(1-Demand(1));
    %foc_firmB =Demand(2) -p(2)*Demand(2)*(1-Demand(2));
    %foc_firmC =Demand(3) -p(3)*Demand(3)*(1-Demand(3));
    foc_firmA = 1 -p(1)*(1-Demand(1));
    foc_firmB = 1 -p(2)*(1-Demand(2));
    foc_firmC = 1 -p(3)*(1-Demand(3));

    foc=[foc_firmA; foc_firmB; foc_firmC];
end