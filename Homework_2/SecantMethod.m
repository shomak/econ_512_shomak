function y = SecantMethod(f, init_guess1, init_guess2, tol, max_iter)
    next_price = (init_guess1*f(init_guess2) - init_guess2*f(init_guess1))/(f(init_guess2)-f(init_guess1));
    iterations=0;
    while abs(f(next_price)) > tol && iterations <max_iter
        init_guess1 = init_guess2; init_guess2 = next_price;
        next_price = (init_guess1*f(init_guess2) - init_guess2*f(init_guess1))/(f(init_guess2)-f(init_guess1));
        iterations=iterations+1;
    end
y = next_price;
end