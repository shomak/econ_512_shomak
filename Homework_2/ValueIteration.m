function [price, iteration] = ValueIteration(init_p,tol,max_iter)
v = -ones(1,3);
iteration = 0;
for i=1:max_iter
   p_new = 1./(ones(3,1)-DemandFunctions(v,init_p));
   error = norm(p_new-init_p);
   if (error<tol) 
      break  
   end
init_p = p_new;
iteration=iteration+1;
end
if (i==max_iter)
   disp('The algorithm did not converge')
end
price = p_new;
end