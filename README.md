This Repository contains all the files and codes for the Empirical Methods course.

Name: Shomak Chakrabarti
Course: ECON 512A: Empirical Methods
PSUID: 975943657
email: sxc615@psu.edu

Files added till date: 
8/30/2017 - Homework 1: Homework1_1.m, Homework1_ECON512.pdf
9/22/2017 - Homework 2 Folder: Contains the files Homework1_2.m, DemandFunctions.m, Bertrand.m, SecantMethod.m, ValueIteration.m, GaussJacobi.m, Homework2_ECON512.pdf. The main program requires CETools to work on.